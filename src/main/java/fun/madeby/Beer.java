package fun.madeby;

public class Beer {
private long id;
private String name;
private double price;
private int stock;
private float alcohol;

public Beer() {
}

public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public int getStock() {
	return stock;
}
public void setStock(int stock) {
	this.stock = stock;
}
public float getAlcohol() {
	return alcohol;
}
public void setAlcohol(float alcohol) {
	this.alcohol = alcohol;
}

@Override
public String toString() {
	return "Beer{" +
		       "id=" + id +
		       ", name='" + name + '\'' +
		       ", price=" + price +
		       ", stock=" + stock +
		       ", alcohol=" + alcohol +
		       '}';
}
}
