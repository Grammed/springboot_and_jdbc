package fun.madeby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class EnterpriseJdbcApplication {

public static void main(String[] args) {
	ConfigurableApplicationContext ctx = SpringApplication.run(EnterpriseJdbcApplication.class, args);
	// we get a copy of JdbcTemplate and NamedParameterJdbc template from the beer repo/dao
	BeerDao beerRepository = ctx.getBean(BeerDaoImpl.class);
	System.out.println(beerRepository.getBeerById(5).toString());
}

}







