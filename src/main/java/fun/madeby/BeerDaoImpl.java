package fun.madeby;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class BeerDaoImpl implements BeerDao {

public final String FIND_BEER_BY_ID = "select * from Beers where Id = ?";

private JdbcTemplate jdbcTemplate;
private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

public JdbcTemplate getJdbcTemplate() {
	return jdbcTemplate;
}

public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
	return namedParameterJdbcTemplate;
}

@Autowired
public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
	this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
}
@Autowired
public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
}

@Override
public Beer getBeerById(int id) {
	Map<String, Object> result = jdbcTemplate.queryForMap(FIND_BEER_BY_ID, id);
	return MapToBeer(result);
}

private Beer MapToBeer(Map<String, Object> result) {
	Beer beer = new Beer();
	beer.setId((int) result.get("Id"));
	beer.setName((String) result.get("Name"));
	beer.setAlcohol(result.get("alcohol") instanceof Float?
		                (float) result.get("Alcohol") :
		                ((Double) result.get("Alcohol")).floatValue()
	);
	beer.setStock((int) result.get("Stock"));
	return beer;
}
}
