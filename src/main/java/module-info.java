open module java {
//    De Api voor het opvragen en verwerken van databankgegevens
	requires java.sql;
//    nodig voor gebruik van java.sql
	requires net.bytebuddy;

//    Spring core modules
	requires spring.core;
	requires spring.context;
	requires spring.beans;
//    Spring Boot modules
	requires spring.boot;
	requires spring.boot.autoconfigure;
//    Module needed for the use of the jdbcTemplate
	requires spring.jdbc;
}